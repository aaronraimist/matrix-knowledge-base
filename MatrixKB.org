#+STARTUP: overview
Hi, welcome to the Matrix Knowledge Base.

Don't be put off by GitLab's horrible attempt at rendering this. It's much better with a proper tool. (See the FAQ FAQ.)

* FAQ :faq:
** FAQ FAQ

Questions about this FAQ and directory itself.

*** What is this?

This is a community-supported collection of FAQs and more on all things [[https://matrix.org][Matrix]]. Its primary use is to serve as botfood for [[https://gitlab.com/Matrixcoffee/FAQBot][FAQBot]], but it could also be rendered as a nicely formatted document.

*** What is it written in?

This document is written in Org mode format. See [[http://orgmode.org]] for more information.

*** What tools are used to author and render this?
**** Authoring

[[http://www.orgzly.com][Orgzly for Android]], which is also available from [[https://f-droid.org/app/com.orgzly][F-Droid]].

**** Synchronizing

MGit: [[https://github.com/maks/MGit]]

**** Rendering :fixme:

(none, so far)

*** Isn't there _already_ a Matrix FAQ?

Yes. This document even links to it at the top of the Matrix section.

*** So why another FAQ?

- Knowledge about Matrix is scattered across many deep-linked documents. This KB brings them together in a comprehensive overview, and links to them.
- This Knowledge Base is not merely a Matrix FAQ: it also has FAQs for some of its popular clients and tools, such as Riot, the IRC bridges, etc. All in one place.
- Using the Org mode format with the Orgzly app lets me more easily curate the questions and answers on my mobile phone.
- It is intended to eventually allow bidirectional conversion of (sections of) this KB, allowing one to wholly import external FAQs, work on them, and then contribute back, keeping everything in sync.
- The Org mode format is very suitable for automatic processing, and a bot could answer questions in Matrix rooms, drawing on this KB for its knowledge. In fact one such bot already exists: [[https://gitlab.com/Matrixcoffee/FAQBot][FAQBot]].

*** How can I add questions to the FAQ?

Good: Visit [[https://gitlab.com/Matrixcoffee/matrix-knowledge-base/edit/master/MatrixKB.org]]. When editing manually, please make sure to obey the org-mode format, including white space and newlines in the right places.

Better: ~git clone https://gitlab.com/Matrixcoffee/matrix-knowledge-base.git~, and use a proper org-mode editor. Maybe even emacs.

*** I have questions or suggestions for this FAQ itself. Where can I go to talk to a human? :_lastresort:

The Matrix Knowledge Base has a dedicated room at [[https://matrix.to/#/#mkb:matrix.org][#matrix-knowledge-base:matrix.org]]. This room is not for general Matrix questions. For those, see the rest of this FAQ.

** Riot :riot:
*** Web and Desktop :web:desktop:
**** Why am I getting 'CORS request rejected' errors? :CORS:CORS_request_rejected:
:PROPERTIES:
:source:   https://matrix.to/#/!DgvjtOljKujDBrxyHk:matrix.org/%2414899602502111244BtYeL:matrix.org
:END:

CORS errors are generally not actually anything to do with CORS, but hide a plain http error, e.g. a 500, which is the real reason why the thing is failing.

However, as plain errors don't have CORS headers, it gives this confusing warning.

That said, CORS errors often happen because some plugin is blocking Riot's requests. For example Noscript, Umatrix or Privacy Badger. Other causes could be (proxy) timeout.

**** What is the best way to contribute a translation? :i18n:fixme:

Matrix and Riot use Weblate as a platform to contribute translations, this platform is available at [[https://translate.riot.im][https://translate.riot.im]]. There is also a channel dedicated to everything regarding translations, you can find it at [[https://matrix.to/#/#riotweb-translations:matrix.org][#riotweb-translations:matrix.org]].

**** How can I change my displayname in one room only, using devtools? :per_room:

There is a graphical walkthrough on how to change your displayname in a single room at [[https://vault.webdevguru.co.uk/Matrix/per-room-nickname/]].

**** Where can I get help with Riot? :_lastresort:

There is an official page with video instructions and FAQ at [[https://about.riot.im/need-help/]].

If that doesn't answer your question, the Web and Desktop versions of Riot have a dedicated room at [[https://matrix.to/#/#riot:matrix.org][#riot:matrix.org]] for user questions. It is also the place for general issues which encompass _all_ Riot platforms. (Web, Desktop, Android, iOS.)

Developers coordinate their efforts in [[https://matrix.to/#/#riot-dev:matrix.org][#riot-dev:matrix.org]].

*** Android :android:
**** How can I input (unicode) emoji? :emoji:

See [[http://www.wikihow.com/Get-Emoji-on-Android]]

**** How do I join an existing room? (For example, #riot-android:matrix.org) :join_room:

From the main ("Messages") screen, Press the magnifying glass and type the room alias into the search field.

**** How do I switch the language?

Riot for Android will automatically switch to your phone's system language, if a translation is available. Manually changing the language is not yet possible, but is planned in [[https://github.com/vector-im/riot-android/issues/1076][#1076]].

**** What is the best way to contribute a translation? :i18n:

*Important*: Before contributing anything, please read [[https://github.com/vector-im/riot-android/blob/master/CONTRIBUTING.rst][Contributing code to Matrix]] carefully and make sure you understand and agree with everything written therein.

After having read [[https://github.com/vector-im/riot-android/blob/master/CONTRIBUTING.rst][Contributing code to Matrix]], the best way to contribute your translation is to send a pull request against the ~develop~ branches of [[https://github.com/vector-im/riot-android][riot-android]] and [[https://github.com/matrix-org/matrix-android-sdk][matrix-android-sdk]].

Translatable strings are in ~res/values/strings.xml~ and ~res/values/array.xml~, and should go to ~res/values-xx/strings.xml~ and ~res/values-xx/array.xml~, respectively, where ~xx~ is the lower-case two-letter language code.

To see an example of what a pull request should look like, take a look at [[https://github.com/vector-im/riot-android/pull/1046][pull #1046: Offering translation into Russian]].

(This pull request is not necessarily an example that stands above all others, and was chosen at random, after some minimal verification.)

*Important*: Translations must be made against the ~develop~ branch, /not/ +master+.

[[https://github.com/matrix-org/matrix-android-sdk][matrix-android-sdk]] needs to be translated as well.

Tip: Android Studio has a
[[https://developer.android.com/studio/write/translations-editor.html][Translations Editor]].

***** Related issues

- [[https://github.com/vector-im/riot-android/issues/811][#811 How can I translate the app?]]
- [[https://github.com/vector-im/riot-android/pull/1046][#1046 Offering translation into Russian]]
- [[https://github.com/vector-im/riot-android/issues/759][#759 translate inteface to pl]]
- [[https://github.com/vector-im/riot-android/pull/660][#660 add portugese language]]
- [[https://github.com/vector-im/riot-android/issues/357][#357 Internationalise vector]]

**** How do push notifications work on Android? :push:

A document explaining this in detail can be found at [[https://github.com/vector-im/riot-android/blob/develop/docs/notifications.md]]

**** What does data save mode do?

Data save mode applies a specific [[https://github.com/matrix-org/matrix-android-sdk/blob/634d7de85e640d650dbc8facf37e40bef6cd0d51/matrix-sdk/src/main/java/org/matrix/androidsdk/sync/EventsThread.java#L56][filter]] so presence updates and typing notifications are filtered out. You will no longer see typing notifications. It does not affect notifications.

You can generally expect Riot to consume less data in this mode. How much is something you'll have to measure and experiment with yourself. To achieve further data savings, you can also disable the sending of typing notifications in settings.

**** What app do I need to send voice messages with Riot?

I recommend [[https://f-droid.org/app/eu.siacs.conversations.voicerecorder]].

**** Can I get access to developer versions of Riot through F-Droid? :f-droid:krombel:
:PROPERTIES:
:alts:     Where is Krombel's repo?
:END:

As a matter of fact, you can! You can add Krombel's repository to [[https://f-droid.org][F-Droid]]: [[https://fdroid.krombel.de/riot-dev/fdroid/repo]]. The repository contains both the F-Droid and the Google Play flavours.

More information at [[https://fdroid.krombel.de]].

**** Where do I report Riot-Android bugs?

[[https://github.com/vector-im/riot-android/issues]]

**** My question isn't answered here. Where can I ask for help? :_lastresort:

Riot-Android version has a dedicated room at [[https://matrix.to/#/#riot-android:matrix.org][#riot-android:matrix.org]] for both user questions and developer talk.

*** iOS :ios:
**** My question isn't answered here. Where can I ask for help? :_lastresort:

Riot-iOS has a dedicated room at [[https://matrix.to/#/#riot-ios:matrix.org][#riot-ios:matrix.org]] for both user questions and developer talk.

*** What commands I can use? :commands:

| command                                          | description                                                                |
|--------------------------------------------------+----------------------------------------------------------------------------|
| =/nick <display_name>=                           | change your display name                                                   |
| =/me <action>=                                   | send the action you are doing -- /me will be replaced by your display name |
| =/join <#room_alias:homeserver.domain>=          | join a room                                                                |
| =/kick <@user_id:homeserver.domain> [<reason>]=  | kick the user                                                              |
| =/ban <@user_id:homeserver.domain> [<reason>]=   | ban the user                                                               |
| =/unban <@user_id:homeserver.domain>=            | unban the user                                                             |
| =/op <@user_id:homeserver.domain> <power_level>= | set the user power level                                                   |
| =/deop <@user_id:homeserver.domain>=             | reset user power level to the room default value                           |

*** How to read a long room topic?

Scroll-drag on mobile, mouse-hover or triple-click to select-all for copying on desktop.

*** What are Riots Terms and Conditions? :tac:

Riot's Terms of Use can be found at [[https://riot.im/tac]]. Questions requiring an authoritative legal answer can be sent to the appropriate contact mentioned on that page.

*** How do I turn encryption on? :encryption:

TL;DR In public rooms, you can't. To enable in private chat, go to settings and flick the encryption setting to on. This cannot be undone.

*** Why can't I search in e2e encrypted rooms? :e2e:encryption:search:

Searching is done by the server. Since e2e messages cannot be decrypted by the server, they cannot be searched either.

** Synapse :synapse:
*** I or one of my users forgot their password. How can I reset it? :password:reset:

See [[https://github.com/matrix-org/synapse/blob/master/README.rst#password-reset]]. (Do read the rest of the README. It is very complete and informative.)

*** How do I enable the built-in web client?

Don't. It's deprecated. Either use Riot at [[https://riot.im/app]] with a (your) custom server, or download a Riot tarball from [[https://github.com/vector-im/riot-web/releases]] and host it yourself.

*** What are forward extremities? :extremities:

Forward extremities are dangling links in a room's DAG. They are known to seriously impact server performance when they accumulate. If you think you have a problem with forward extremities, please see [[https://github.com/matrix-org/synapse/issues/1760]].

*** What is the recommended setup for Synapse?

The recommended way to set up Synapse is discussed and clarified in [[https://github.com/matrix-org/synapse/issues/2438]].

*** How to install Synapse?

Try PC-Admin's [[https://github.com/PC-Admin/PC-Admin-s-Synapse-Setup-Guide][very complete Debian 9 walkthrough including TURN setup]]. The community recommends against most other guides, including digitalocean's, as they make you replace the self-signed certificate and/or proxy the federation port for no good reason. And of course you should thoroughly familiarize yourself with [[https://github.com/matrix-org/synapse/blob/master/README.rst][Synapse's own README]].

You can also ask me how to install Synapse as a Docker container.

*** Is there a setup guide for Synapse?

Try PC-Admin's [[https://github.com/PC-Admin/PC-Admin-s-Synapse-Setup-Guide][very complete Debian 9 walkthrough including TURN setup]]. The community recommends against most other guides, including digitalocean's, as they make you replace the self-signed certificate and/or proxy the federation port for no good reason. And of course you should thoroughly familiarize yourself with [[https://github.com/matrix-org/synapse/blob/master/README.rst][Synapse's own README]].

You can also ask me how to install Synapse as a Docker container.

*** How to install Synapse as a Docker container?

If you want to set up a new server, this is probably the easiest way and will set everything up correctly: [[https://github.com/spantaleev/matrix-docker-ansible-deploy]]

See also [[https://hub.docker.com/r/matrixdotorg/synapse/]]

*** How do I set up TURN? :turn:coturn:

See [[https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.rst][Synapse's TURN Howto]]. For a clear example configuration, see Max's [[https://gist.github.com/maxidor/2b0acc2e707ae9a2d6d0267026a1024f][working VoIP configuration]]. Finally, there is a [[https://github.com/PC-Admin/PC-Admin-s-Synapse-Setup-Guide][complete Synapse setup guide]] including coturn setup.

*** How do I configure secondary_directory_servers in homeserver.yaml?

This configuration key hasn't been supported since September 2016. ([[https://github.com/matrix-org/synapse/commit/413138112379594bf9290576d44c365af612817d#diff-7604431227e17cc581f690570dfacca6][Source]])

Your best bet is to set up and deploy a custom Riot webclient and add those extra room directories there.

*** How do I make an existing user a Server Admin?
:PROPERTIES:
:source:   https://matrix.to/#/!HsxjoYRFsDtWBgDQPh:matrix.org/%241527689668886qCUHb:kamax.io
:credits:  @max:kamax.io
:END:

Currently, the only way is to manually toggle the admin flag in the database. The query is:

~update users set admin = 1 where name = '<matrix ID here>';~

Note that a Server Admin is not a Room Admin, and Server Admins have very limited power over rooms.

*** Where can I find a sample homeserver.yaml?

[[https://github.com/matrix-org/package-synapse-debian/blob/debian/debian/homeserver.yaml]] is probably the best ready-made example, and running "~python -m synapse.app.homeserver --generate-config~" should generate the most up to date example config.

Relevant issues: [[https://github.com/matrix-org/synapse/issues/1433][1433]] , [[https://github.com/matrix-org/synapse/issues/2939][2939]], [[https://github.com/matrix-org/synapse/issues/1464][1464]] and [[https://github.com/matrix-org/synapse/issues/1303][1303]].

*** Is there an admin or control panel for Synapse?

There are no mature solutions yet. There is a stub project at [[https://github.com/colonelkrud/Matrix-Control-Panel]]. You can help the project by submitting feature requests to [[https://feathub.com/colonelkrud/Matrix-Control-Panel]].

Another project is [[https://github.com/aminhusni/Trinity_Matrix_Management_GUI]].

*** How can I expire old events from Synapse?

Try [[https://pypi.python.org/pypi/synpurge/4][synpurge]] or [[https://github.com/djmaze/synapse-purge][synapse-purge]].

*** Why is it okay to have a self-signed certificate for the federation endpoint?

[[https://github.com/matrix-org/matrix-doc/pull/1711][MSC1711]]
goes into quite a lot of detail about that.

*** My Synapse question isn't answered here. Where can I ask for help? :_lastresort:

General help with setting up and configuring Synapse, as well as basic troubleshooting, can be had from [[https://matrix.to/#/#synapse:matrix.org][#synapse:matrix.org]].

If you think you found a bug, don't forget to check the [[https://github.com/matrix-org/synapse/issues][bug tracker]] to see if others are having the same problem.

Deep technical and programming-related questions are best taken to [[https://matrix.to/#/#matrix-dev:matrix.org][#matrix-dev:matrix.org]]

** Dendrite :dendrite:
*** My question isn't answered here. Where can I ask for help? :_lastresort:

Rooms will be listed here when Dendrite is usable.

** Matrix
*** The official Matrix FAQ

- [[https://matrix.org/docs/guides/faq.html]]

*** What is the Matrix?

Unfortunately, no one can be told what the Matrix is. You have to see it for yourself.

That said, see [[https://matrix.org]] for an attempt at explaining it.

*** Who should I contact in case of abuse? :abuse:

Send e-mail to _abuse_ _@_ _matrix.org_

*** Who should I contact about security issues? :security:

Send e-mail to _security_ _@_ _matrix.org_

*** I can see everyone's devices! Isn't that a privacy issue?
:PROPERTIES:
:source:   https://matrix.to/#/#matrix:matrix.org/%241489661409560128pTiCo:matrix.org
:END:

Yes, this is a known metadata leak. Other than renaming your devices, there isn't much that can be done about it.

Ironically, e2e encryption reduces privacy in some ways. Since Matrix e2e encrypts to each device separately, it needs to know which devices those are. This is a privacy vs. features tradeoff. Most competing encrypted messengers don't allow multiple devices.

*** How does the "mxc://" protocol work?

~mxc://<server>/<content>~ is just shorthand for ~https://<yourserver>/_matrix/media/v1/download/<server>/<content>~

The spec is at [[https://matrix.org/docs/spec/client_server/r0.3.0.html#id67]].

*** Where is the latest Matrix spec?

Follow the appropriate links from [[https://matrix.org/docs/spec]]. Please don't overlook the [[https://matrix.org/docs/spec/appendices.html][appendices]] which are an integral part of the spec and contain important information.

The latest unstable client-server specification can separately be found at [[https://matrix.org/docs/spec/client_server/unstable.html]].

*** Where can I find revision r0.1.0 of the client-server API?

[[https://matrix.org/docs/spec/client_server/r0.1.0.html]]

*** Where can I find revision r0.2.0 of the client-server API?

[[https://matrix.org/docs/spec/client_server/r0.2.0.html]]

*** Where can I find revision r0.3.0 of the client-server API?

[[https://matrix.org/docs/spec/client_server/r0.3.0.html]]

*** Where can I find revision r0.4.0 of the client-server API?

[[https://matrix.org/docs/spec/client_server/r0.4.0.html]]

*** Where is the unstable client-server spec?

The latest unstable client-server specification can be found at [[https://matrix.org/docs/spec/client_server/unstable.html]].

*** What's the difference between a room and a direct chat?

There is no real difference between the two. Direct (or private) chat is just a flag that causes clients such as Riot to treat these rooms specially. Some bridges may also handle them differently.

*** Why am I showing as offline?

You probably have an account on matrix.org. Presence on matrix.org accounts is disabled because the server is suffering from high load. If this bothers you, your best bet is to look at the [[https://www.hello-matrix.net/public_servers.php][list of public matrix servers]] and get an account on a different server.

*** Why are my friends showing as offline?

Your friends probably have an account on matrix.org. Presence on matrix.org accounts is disabled because the server is suffering from high load. If this bothers you, your best bet is to look at the [[https://www.hello-matrix.net/public_servers.php][list of public matrix servers]] and get everyone to set up an account on a different server.

*** Where is the list of public matrix servers? :public:servers:

An unofficial list of public matrix servers can be found at [[https://www.hello-matrix.net/public_servers.php]]. If you have questions or comments about the list, please join #hello-matrix:matrix.org.

*** What is Try Matrix Now?

Try Matrix Now is a global directory of projects using Matrix in some way. It can be found at [[https://matrix.org/docs/projects/try-matrix-now.html]].

*** How can I add my project to Try Matrix Now?

To add your project, make a copy of the [[https://github.com/matrix-org/matrix.org/blob/master/jekyll/_posts/projects/template.md][template]] and give it the name of your project prefixed with today's date. For some reason jekyll is picky about file names, so make sure it meshes with the rest of the files.

Now fill in the details and send a pull request to have it added to Try Matrix Now. (And to [[https://gitlab.com/Matrixcoffee/FAQBot][FAQBot]].)

*** Is this the right place to ask about GSoC? :GSoC:

Try #gsoc:matrix.org

*** How can I test whether my server is properly set up for federation? :federation:tester:

Test your server by appending its domain to: ~https://matrix.org/federationtester/api/report?server_name=~

New: A nice wrapper GUI for the above at [[https://neo.lain.haus/fed-tester/]]

*** My question isn't answered here. Where can I ask for help? :_lastresort:

General Matrix questions can be asked in [[https://matrix.to/#/#matrix:matrix.org][#matrix:matrix.org]]. If you're building on top of Matrix, please join [[https://matrix.to/#/#matrix-dev:matrix.org][#matrix-dev:matrix.org]].

*** Crypto :crypto:e2e:
**** What is e2e encryption?

See Wikipedia: [[https://en.wikipedia.org/wiki/End-to-end_encryption]]. Matrix end-to-end encryption is based on the Megolm protocol.

**** What is Olm? :olm:

Olm is a [[https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm][Double Ratchet Algorithm]] used to encrypt communications between two endpoints. Please note that Megolm, not Olm, is used in Matrix, even in private chats between two people. For more information about Olm, see the [[https://matrix.org/docs/spec/olm.html][Olm spec]].

**** What is Megolm? :megolm:

Megolm is a group ratchet encryption protocol for communication between two or more parties, built on top of Olm. If you are using crypto in in your favourite glossy Matrix client, you are using Megolm. For more information about Megolm, see the [[https://matrix.org/docs/spec/megolm.html][Megolm spec]].

**** Has Olm been cryptographically reviewed? :review:

Yes, the [[https://www.nccgroup.trust/us/][NCC Group]] have reviewed libolm version 1.3.0. The [[https://www.nccgroup.trust/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf][olm cryptographic review]] was [[https://www.nccgroup.trust/us/our-research/matrix-olm-cryptographic-review/][published]] on 18 November 2016. This review was funded by the [[https://www.opentech.fund/][Open Technology Fund]].

**** How can I add e2e support to my own project? :implementation:

Please consult the [[https://matrix.org/docs/guides/e2e_implementation.html][Official e2e implementation guide]].

**** Can you give me more background on matrix e2e? :background:

There's some information about how Olm/Megolm came to be (among various other random things) in [[https://matrix.org/blog/2016/11/21/matrixs-olm-end-to-end-encryption-security-assessment-released-and-implemented-cross-platform-on-riot-at-last/][this matrix.org blog post]].

**** Why is libolm not on GitHub? :libolm:export:

Placing libolm on GitHub would constitute 'importing' it to the United States, and from there, re-[[https://en.wikipedia.org/wiki/Export_of_cryptography_from_the_United_States][exporting]] it all over the world. To avoid this, libolm is kept in a private git repository outside of the United States. You can find libolm at [[https://matrix.org/git/olm]] instead.

**** What is the problem with crypto export? :export:

The export of cryptographic technology and devices from the United States was severely restricted by U.S. law until 1992, but was gradually eased until 2000. Some restrictions still remain. [[https://en.wikipedia.org/wiki/Export_of_cryptography_from_the_United_States]]

**** Where is the e2e implementation guide?

The official e2e implementation guide is at [[https://matrix.org/docs/guides/e2e_implementation.html]].

** Bridges :bridge:
*** What's a plumbed room? What's a portal room? What types of bridges are there? :plumbed:portal:puppet:

[[https://matrix.org/blog/2017/03/11/how-do-i-bridge-thee-let-me-count-the-ways/]] has a detailed overview.

*** IRC :irc:
**** How does this whole IRC bridging thing work?

[[https://matrix.org/blog/2017/03/14/an-adventure-in-irc-land/]] goes into detail about this.

**** What is a bridge bot? :TESTME:

On joining an IRC room for the first time, you should be invided by the bridge bot, to inform you that you have been bridged to IRC.

The bot will inform you about changes in your IRC status,  and you can give it some commands to control your IRC session. Say ~!help~ to the bot to see a list of them.

Be aware that every IRC network has its own bot, and you need to talk to the right one. See [[#bridged-networks]] for the list of IRC networks and the appropriate bridge bots.

**** How do I join an IRC channel with a key? (+k) :key:TESTME:

In a private conversation with the [[#bridge-bot][bridge bot]], say ~!join [server] <#channel> <key>~.

If this gives you an error ~err_badchannelkey~, type ~/markdown off~ in the bridge bot room, and then try the ~!join~ command again.

**** Where is the list of bridged IRC networks?

The community-maintained list is [[https://github.com/matrix-org/matrix-appservice-irc/wiki/Bridged-IRC-networks][on the wiki]].

The /official/ IRC network (wish)list can be [[https://github.com/matrix-org/matrix-appservice-irc/issues/208][found here]].

**** Where is the official IRC network bridging wishlist?

The official IRC network wishlist can be [[https://github.com/matrix-org/matrix-appservice-irc/issues/208][found here]].

**** How do I disconnect from IRC? :disconnect:quit:
:PROPERTIES:
:alts:     How do I quit IRC?
:END:

You can force the bridge to disconnect you from IRC by saying !quit to the bridge bot. Take note that this will kick you from all rooms that are bridged to that irc network. This might include rooms you didn't even realize were bridged to irc.

**** How do I talk to NickServ? :nickserv:

Firstly, look up which of the NickServs you want to talk to in the [[https://github.com/matrix-org/matrix-appservice-irc/issues/208][list of bridged networks]].

For example, for Freenode this would be @freenode_nickserv:matrix.org.

Then you should invite it for a private chat, wait until it joins, and just talk to it. Try saying "help".

*Important*: You must not write ~/msg NickServ~ in front of the commands, as you are not using an IRC client but a Matrix client!

**** How to change my IRC nickname?

Find the right [[https://github.com/matrix-org/matrix-appservice-irc/wiki/Bridged-IRC-networks][bridge bot]] and say: ~!nick newnick~

If that adds some form of ~[m]~ after your desired nickname, or it changes to Guest12345, it means the nick is already taken, or registered. Choose a different one.

**** Where can I get help with IRC bridges? :_lastresort:

Irc bridges have a dedicated room at [[https://matrix.to/#/#irc:matrix.org][#irc:matrix.org]] for both user questions and developer talk.

All other bridges can be discussed at [[https://matrix.to/#/#bridges:matrix.org][#bridges:matrix.org]].

** Other
*** How To Ask Questions The Smart Way?

If you'd like to get better answers to your questions, sometimes even without asking, take some time to read [[http://www.catb.org/esr/faqs/smart-questions.html][How To Ask Questions The Smart Way]].

*** How does matrix.org keep their official Twitter and Mastodon in sync?

By means of [[https://github.com/AmauryCarrade/MastodonToTwitter][MastodonToTwitter]].

* Keywords (old, to be reworked)

This contains the keywords that were taught to *mubot* at some point. An update to mubot means this information is now "lost". It is being reconstructed here, for the purpose of eventually integrating it into the FAQ/Directory properly.

** 3pid

In the context of Matrix, any identifier which is not a Matrix ID. This includes e-mail addresses, phone numbers, Facebook accounts, etc.

** alias :missing:
** balancing interop and privacy

[Slides] Matthew talks about the challenge of balancing interoperability and privacy: [[https://matrix.org/~matthew/2016-12-22%20Matrix%20Balancing%20Interop%20and%20Privacy.pdf]]

** What is the Matrix Code of Conduct? :code:conduct:

This code of conduct outlines our expectations for participants within the Matrix community, as well as steps for reporting unacceptable behaviour. We are committed to providing a welcoming and inspiring community for all, and expect our code of conduct to be honoured. Anyone who violates this code of conduct may be banned from the community.

- [[https://matrix.org/docs/guides/code_of_conduct.html]]

** desktop app :missing:
** exul slides :missing:
** gc :missing:
** id :missing:
** identity server :missing:
** matrix on freenode :missing:
** migrating to postgres :missing:
** mubot

[[https://github.com/davidar/hubot-matrix]]

#mubot:davidar.io

** mxid :missing:
** network list :missing:
** olm :missing:
** receipt :missing:
** reindex

TIL that PostgreSQL may have internal fragmentation in the btrees used for indexes, and that can cause VACUUM FULL to make the database bigger... which can be solved by issuing a REINDEX DATABASE - [[https://matrix.to/#/!cURbafjkfsMDVwdRDQ:matrix.org/%2414835668811638615xXMYC:matrix.org]]

** screen sharing :missing:
** setting up federation

[[https://github.com/matrix-org/synapse#setting-up-federation]] has words on it.

** slack hook

See [[https://matrix.to/#/!svJUttHBtRMdXmEhEy:matrix.org/$14836112541869035fxcVU:matrix.org]]

** those little circles :missing:
** upgrade script :missing:
** webrtc tutorial
*** Basics

[[https://www.html5rocks.com/en/tutorials/webrtc/basics/]]

*** Infrastructure

[[https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/]]

** xmpp bridge
** yzord :missing:
* Processing Queue

Things which should probably be added, but need to be reworked, or need a closer look.

(This section is mainly an artifact of me not being able to file or access issues offline.)

** What happens if I mark a room as "direct chat"? Does it effect anything? :needsanswer:
** A shell script for tagging rooms

[[https://gist.github.com/turt2live/697d5b3781fb7ab7e11d704ea90e3dbe]]

** There are a couple of community-written users guides linked to in this issue as well: [[https://github.com/vector-im/riot-web/issues/3308]]
** hmm, right, I don't think there's the concept of an admin user on matrix

there is, but it is very limited
you must enable it by editing your user row on the database
and there is only two api two delete old history and old medias

is that documented somewhere?

yep: [[https://github.com/matrix-org/synapse/tree/master/docs/admin_api]]

[[https://matrix.to/#/!svJUttHBtRMdXmEhEy:matrix.org/%24149022212811cUBkh:orbstheorem.ch]]
#bridges:matrix.org

** [[https://github.com/matrix-org/synapse/wiki#i-have-a-problem-with-my-server-can-i-just-delete-my-database-and-start-again]]
** i'm still wondering if our IPs are protected on Riot or not.. no one can obtain it right?

Only the server admin can see the IP (of course).
Nobody else can, the ip is not propagated in any way.
well, it is if you make a 1:1 call
Oh right, that's true.
Voice and video calls are peer-to-peer, so of necessity the IPs have to be exchanged.
Worse is that it sends all known interface addresses, including local (private) network addresses.
Which is actually awesome if you're actually placing a call to someone on the same private network.
But also cause for (privacy) concerns.
If it is of great concern, the solution is to neither place nor accept voice and video calls.

[[https://matrix.to/#/!cURbafjkfsMDVwdRDQ:matrix.org/%2414902634851380909LwydX:matrix.org]]
#matrix:matrix.org

** How to get my access token ?

[[https://matrix.to/#/#matrix:matrix.org/%2414902578321344171PDCNZ:matrix.org]]

** I found the icons missing after the nginx reverse proxy, any sample nginx config for riot-web?

FYI. It caused by X-Frame-Options set to deny. Now I use the SAMEORIGIN and problem solved.

[[https://matrix.to/#/!DgvjtOljKujDBrxyHk:matrix.org/%2414903176841958169Wkaps:matrix.org]]
#riot:matrix.org

** Add GCM replacements

Add here and to riot-android#1066

